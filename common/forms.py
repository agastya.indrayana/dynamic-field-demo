from django import forms
from .models import Attribute, Patient


class AttributeForm(forms.ModelForm):
    class Meta:
        model = Attribute
        fields = ["name", "active", "max_length"]


class PatientForm(forms.ModelForm):
    class Meta:
        model = Patient
        exclude = ["attributes"]


class PattientAttributeForm(forms.Form):

    def __init__(self, *args, **kwargs):
        attributes = Attribute.objects.filter(active=True)
        instance = kwargs.pop("instance",None)
        super().__init__(*args, **kwargs)

        for attribute in attributes:
            field_name = attribute.normal_name
            field = forms.CharField(widget=forms.TextInput(
                attrs={'style': ''}), required=False)
            self.fields[field_name] = field


        for field_name, field in self.fields.items():
            if instance != None and instance.attributes != None:
                field.initial = instance.attributes.get(field_name,"")

            field.widget.attrs['class'] = 'widget-control'
