from django.urls import path
from .views import indexView, listAttributeView, editAttributeView, editPatientView


app_name = "common"


urlpatterns = [
    path('', indexView.as_view(), name='index'),
    path('edit-patient/<int:id>/', editPatientView.as_view(), name='edit-patient'),
    path('list-attribute/', listAttributeView.as_view(), name='list-attribute'),
    path('edit-attribute/<int:id>/', editAttributeView.as_view(), name='edit-attribute'),
]
