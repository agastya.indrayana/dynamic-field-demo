from .models import Attribute, Patient
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

@receiver(post_save, sender=Patient)
def addDefaultAttributeForNewObj(sender, instance, created, **kwargs):
    if created:
        attributes = Attribute.objects.all()
        for attribute in attributes:
            if instance.attributes.get(attribute.normal_name,'') == '':
                instance.attributes[attribute.normal_name] = ''
        instance.save()

@receiver(post_save, sender=Attribute)
def addNewAttribute(sender, instance, created, **kwargs):
    if created:
        objects = Patient.objects.all()
        for obj in objects:
            obj.attributes[instance.normal_name] = ''
            obj.save()

@receiver(post_delete, sender=Attribute)
def deleteAttribute(sender, instance, **kwargs):
    objects = Patient.objects.all()
    for obj in objects:
        obj.attributes.pop(instance.normal_name)
        obj.save()