from django.shortcuts import render, HttpResponseRedirect, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import (
    CreateView, UpdateView, DetailView, TemplateView, View, DeleteView)
from .forms import AttributeForm, PatientForm, PattientAttributeForm
from .models import Attribute, Patient


class indexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(indexView, self).get_context_data(**kwargs)
        patients = Patient.objects.all()
        attributes = Attribute.objects.filter(active=True)

        rows = []
        for patient in patients:
            row = [patient.name, patient.email]
            for attribute in attributes:
                key = attribute.normal_name
                value = patient.attributes.get(key, "")
                row.append(value)

            row = {"data":row, "id":patient.id}
            rows.append(row)
        context["rows"] = rows
        context["attributes"] = attributes
        return context

    def post(self, request, *args, **kwargs):
        form = PatientForm(request.POST)
        attribute_form = PattientAttributeForm(request.POST)
        if form.is_valid() and attribute_form.is_valid():
            patient = form.save(commit=False)
            patient_attribute = attribute_form.cleaned_data
            patient.attributes = patient_attribute
            patient.save()
            return redirect(reverse("common:index"))
        else:
            context = self.get_context_data()
            context["form"] = form
            context["attribute_form"] = attribute_form
            return self.render_to_response(context=context)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context["form"] = PatientForm()
        context["attribute_form"] = PattientAttributeForm()
        return self.render_to_response(context=context)


class editPatientView(TemplateView):
    template_name = "editPatient.html"

    def post(self, request, *args, **kwargs):
        patient = get_object_or_404(
            Patient, id=kwargs.get("id"))
        form = PatientForm(request.POST, instance=patient)
        attribute_form = PattientAttributeForm(request.POST, instance=patient)
        if form.is_valid() and attribute_form.is_valid():
            patient = form.save(commit=False)
            patient_attribute = attribute_form.cleaned_data
            patient.attributes = patient_attribute
            patient.save()
            return redirect(reverse("common:index"))
        else:
            context = self.get_context_data()
            context["form"] = form
            context["attribute_form"] = attribute_form
            return self.render_to_response(context=context)

    def get(self, request, *args, **kwargs):
        context = super(editPatientView, self).get_context_data(**kwargs)
        patient = get_object_or_404(
            Patient, id=kwargs.get("id"))
        context["form"] = PatientForm(instance=patient)
        context["attribute_form"] = PattientAttributeForm(instance=patient)
        return self.render_to_response(context=context)


class listAttributeView(TemplateView):
    template_name = "listAttribute.html"

    def get_context_data(self, **kwargs):
        context = super(listAttributeView, self).get_context_data(**kwargs)
        context["attributes"] = Attribute.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        form = AttributeForm(request.POST)
        if form.is_valid():
            attribute = form.save()
            return redirect(reverse("common:list-attribute"))
        else:
            context = self.get_context_data()
            context["form"] = form
            return self.render_to_response(context=context)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context["form"] = AttributeForm()
        return self.render_to_response(context=context)


class editAttributeView(TemplateView):
    template_name = "editAttribute.html"

    def post(self, request, *args, **kwargs):
        attribute = get_object_or_404(
            Attribute, id=kwargs.get("id"))
        old_attribute_name = attribute.normal_name
        form = AttributeForm(request.POST, instance=attribute)
        if form.is_valid():
            attribute = form.save()
            if 'name' in form.changed_data:
                patients = Patient.objects.all()
                for patient in patients:
                    if patient.attributes.get(old_attribute_name, '') == '':
                        patient.attributes[attribute.normal_name] = ''
                    elif patient.attributes.get(old_attribute_name, '') != '':
                        patient.attributes[attribute.normal_name] = patient.attributes.pop(
                            old_attribute_name)
                    patient.save()
            return redirect(reverse("common:list-attribute"))
        else:
            context = self.get_context_data()
            context["form"] = form
            return self.render_to_response(context=context)

    def get(self, request, *args, **kwargs):
        context = super(editAttributeView, self).get_context_data(**kwargs)
        attribute = get_object_or_404(
            Attribute, id=kwargs.get("id"))
        context["form"] = AttributeForm(instance=attribute)
        return self.render_to_response(context=context)
