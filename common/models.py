from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField

class Attribute(models.Model):
    name = models.CharField(max_length=100, blank= False)
    active = models.BooleanField(blank=False, default=True)
    max_length = models.PositiveIntegerField(blank=False)

    @property
    def normal_name(self):
        return self.name.lower().replace(" ","_")

    def clean(self):
        super(Attribute, self).clean()
        # Implement data validation

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Patient(models.Model):
    name = models.CharField(max_length=100, blank= False)
    email = models.EmailField(blank= False)
    attributes = JSONField(blank=True, null=True)