# Dynamic field

A demo on how dynamic attribute can be implemented on exsisting model by adding JSONField, with a model to store the attribute name and other information such as status of attribute(active/non-active) and max length of said attribute value. Also included are form that are generated from dynamic attribute that has been registered.


## Installation

```bash
py -m venv env
env\Scripts\activate.bat
pip install -r requirements.txt
```

## Note
Make sure you got your local postgres server running and the change the DB credential in settings.py

## Usage

```python
py manage.py migrate
py manage.py runserver
```

## License
[MIT](https://choosealicense.com/licenses/mit/)